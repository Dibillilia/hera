__author__ = '18DellingerA'

from .linear_sklearn import gen_sklearn_linear as sk_linear
from .powerful_sklearn import gen_sklearn_powerful as sk_powerful

from .keras_models import gen_keras as keras

from .dummy import gen_dummy as dummy
from .naive import gen_naive as naive